require 'rubygems'

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup'

Bundler.require

module ITS
end

require_relative './its/call'
require_relative './its/center'
require_relative './its/customer'
require_relative './its/invoice'
