module ITS
  class Center
    attr_reader :calls, :customers

    # Storage in memory
    def initialize(customers = [], calls = [])
      @customers = customers
      @calls = calls
    end

    def create_customer(phone, full_name)
      new_customer = Customer.new(phone, full_name)
      @customers << new_customer
      new_customer
    end

    def charge_call(customer_id, destination, started_at, ended_at)
      return if @customers.find {|customer| customer_id == customer.id }.nil?

      new_call = Call.new(customer_id, destination, started_at, ended_at)
      @calls << new_call
      new_call
    end

    def generate_invoice_for(customer_id, month)
      return if @customers.find {|customer| customer_id == customer.id }.nil?

      calls = @calls.select { |call| call.customer_id == customer_id }.
                     select { |call| call.month == month }
      Invoice.new(customer_id, calls, month, Call::Calculator)
    end

    def generate_invoices_for(month)
      @customers.map do |customer|
        generate_invoice_for(customer.id, month)
      end
    end
  end
end
