module ITS
  class Call
    attr_reader :customer_id, :destination, :ended_at, :started_at

    def initialize(customer_id, destination, started_at, ended_at)
      @customer_id = customer_id
      @destination = destination
      @started_at  = started_at
      @ended_at    = ended_at
    end

    def type
      RateStrategySelector.for(self).to_s
    end

    def month
      @started_at.month
    end

    def period_in_mins
      (@ended_at - @started_at) / 60
    end

    private

    class Calculator
      attr_reader :call

      def initialize(call)
        @call = call
      end

      def perform
        RateStrategySelector.for(@call).invoke.round(2)
      end
    end

    module RateStrategySelector
      extend self

      def for(call)
        calculator = case call.destination
        when /^5411/ then LocalRateCalculator
        when /^54/  then NationalRateCalculator
        else
          InternationalRateCalculator
        end

        calculator.new(call)
      end
    end

    class LocalRateCalculator
      attr_reader :call

      LOCAL_RATE_8_to_20 = 0.20
      LOCAL_RATE_20_to_8 = 0.10
      WEEKEND_RATE = 0.10

      def initialize(call)
        @call = call
      end

      def invoke
        rate = case @call.started_at.wday
        when 6..7 then WEEKEND_RATE
        else
          case @call.started_at.hour
            when 8..20 then LOCAL_RATE_8_to_20
            else
              LOCAL_RATE_20_to_8
            end
        end

        rate * @call.period_in_mins
      end

      def to_s
        'Local'
      end
    end


    class NationalRateCalculator
      attr_reader :call

      RATES = { '3755' => 2 }

      def initialize(call)
        @call = call
      end

      def invoke
        RATES[@call.destination[2..5]] * @call.period_in_mins
      end

      def to_s
        'National'
      end
    end

    class InternationalRateCalculator
      attr_reader :call

      RATES = { '55' => 5 }

      def initialize(call)
        @call = call
      end

      def invoke
        RATES[@call.destination[0..1]] * @call.period_in_mins
      end

      def to_s
        'International'
      end
    end
  end
end
