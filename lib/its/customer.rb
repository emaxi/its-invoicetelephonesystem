module ITS
  class Customer
    attr_accessor :id, :full_name, :phone

    def initialize(phone, full_name)
      @id = SecureRandom.uuid
      @full_name = full_name
      @phone = phone
    end
  end
end
