module ITS
  class Invoice
    attr_accessor :customer_id
    attr_reader :calls, :month, :calculator

    MONTHLY_SUBSCRIPTION = 100

    def initialize(customer_id, calls, month, calculator)
      @customer_id = customer_id
      @calls = calls
      @month = month
      @calculator = calculator
    end

    def charged
      calculate_calls + MONTHLY_SUBSCRIPTION
    end

    def to_s
      %{
        Customer: #{@customer_id}
        Month: #{@month}
        Calls: #{@calls.map { |call| call_detail(call) }}
        Subscription: #{MONTHLY_SUBSCRIPTION}
        Charged: #{charged}
      }
    end

    private

    def calculate_call(call)
      calculator.new(call).perform
    end

    def calculate_calls
      @calls.map do |call|
        calculate_call(call)
      end.reduce(0, &:+)
    end

    def call_detail(call)
      "#{call.destination} (#{call.type}) #{calculate_call(call)}"
    end
  end
end
