require_relative './lib/its'

# Creation of customers
its = ITS::Center.new
customer1 = its.create_customer('312312323', 'Jose Alberto')
customer2 = its.create_customer('312312321', 'Pepe Uriarte')
customer3 = its.create_customer('31231231', 'Justiniano Mandrake')
customer4 = its.create_customer('31231231', 'Magolla Fernandez')


# Charging calls
its.charge_call(customer1.id, '55114646393', Time.now - 60*60, Time.now - 60 * 30) # International
its.charge_call(customer1.id, '55114646393', Time.now - 60*60, Time.now - 60 * 10) # International
its.charge_call(customer2.id, '54375546393', Time.now - 60*60, Time.now - 60 * 30) # National
its.charge_call(customer3.id, '54114646393', Time.now - 60*60, Time.now - 60 * 30) # Local
its.charge_call(customer4.id, '54114646393', Time.now - 31 * 24 * 60 * 60, Time.now - 31 * 24 * 60 * 30) # Local based on previous requested month

# Generation of invoices
invoices = its.generate_invoices_for(Time.now.month)

invoices.each do |invoice|
  puts invoice
end

