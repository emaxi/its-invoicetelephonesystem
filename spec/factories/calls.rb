FactoryGirl.define do
  factory :call, class: ITS::Call do
    skip_create

    customer_id { create(:customer).id }
    destination { Faker::PhoneNumber.phone_number }
    started_at { Time.now }
    ended_at { Time.now + 3600 }

    initialize_with { new(customer_id, destination, started_at, ended_at) }

    trait :international do
      destination { '55114646393' }
    end

    trait :national do
      destination { '54375546393' }
    end

    trait :local do
      destination { '54114646393' }
    end

    trait :wday_within_8_20 do
      started_at { Time.new(2008,6,20, 13,30,0) }
      ended_at { Time.new(2008,6,20, 15,30,0) }
    end

    trait :wday_within_20_8 do
      started_at {  Time.new(2008,6,20, 0,30,0) }
      ended_at {  Time.new(2008,6,20, 1,30,0) }
    end

    trait :weekend do
      started_at {  Time.new(2008,6,21, 0,30,0) }
      ended_at {  Time.new(2008,6,21, 1,30,0) }
    end

    trait :one_month_later do
      started_at { Time.now + 60 * 60 * 24 * 30 }
      ended_at { Time.now + 3600 + 60 * 60 * 24 * 30 }
    end
  end
end
