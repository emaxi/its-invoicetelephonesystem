FactoryGirl.define do
  factory :customer, class: ITS::Customer do
    skip_create

    id { SecureRandom.uuid }
    phone { Faker::PhoneNumber.phone_number }
    full_name { Faker::Name.name }

    initialize_with { new(phone, full_name) }
  end
end
