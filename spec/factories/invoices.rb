FactoryGirl.define do
  customer_id = FactoryGirl.create(:customer).id

  factory :invoice, class: ITS::Invoice do
    skip_create

    customer_id { create(:customer).id }
    calls { create_list(:call, 2, :international, customer_id: customer_id) }
    month { rand(12) }
    calculator { ITS::Call::Calculator }

    initialize_with { new(customer_id, calls, month, calculator) }
  end
end
