require 'spec_helper'

module ITS
  describe Center do
    let(:its) { ITS::Center.new }

    describe '.create_customer' do
      let(:phone) { Faker::PhoneNumber.phone_number }
      let(:full_name) { Faker::Name.name }
      let(:its) { ITS::Center.new }
      let!(:customer) { its.create_customer(phone, full_name) }

      it { expect(its.customers.size).to eq 1 }
      it { expect(its.customers.first).to eq customer }
      it { expect(customer.id).to_not be_nil }
      it { expect(customer.phone).to eq phone }
      it { expect(customer.full_name).to eq full_name }
    end

    describe '.charge_call' do
      let(:customer) { create(:customer) }
      let(:destination) { Faker::PhoneNumber.phone_number }
      let(:started_at) { Time.now }
      let(:ended_at) { Time.now + 3600 }
      before do
        its.customers << customer
        Timecop.freeze
      end
      let!(:call) do
        its.charge_call(customer.id, destination, started_at, ended_at)
      end

      it { expect(its.calls.size).to eq 1 }
      it { expect(its.calls.first).to eq call }
      it { expect(call.customer_id).to eq customer.id }
      it { expect(call.started_at).to eq started_at }
      it { expect(call.ended_at).to eq ended_at }

      after { Timecop.return }
    end

    describe '.generate_invoice_for' do
      let!(:call2) { create(:call, :national, customer_id: customer.id) }
      let!(:call3) { create(:call, :national, customer_id: customer.id) }
      let(:customer) { create(:customer) }
      let(:month) { call1.month }
      context 'matching three calls' do
        let!(:call1) { create(:call, :international, customer_id: customer.id) }
        before do
          its.customers << customer
          its.calls << call1 << call2 << call3
        end
        let(:invoice) { its.generate_invoice_for(customer.id, month) }
        let(:expected) do
          Call::Calculator.new(call1).perform +
            Call::Calculator.new(call2).perform +
            Call::Calculator.new(call3).perform + Invoice::MONTHLY_SUBSCRIPTION
        end

        it { expect(invoice.calls).to eq [call1, call2, call3] }
        it { expect(invoice.customer_id).to eq customer.id }
        it { expect(invoice.month).to eq month }
        it { expect(invoice.charged).to eq(expected) }
      end

      context 'matching two calls from same customer and reject one from another customer' do
        let(:customer2) { create(:customer) }
        let!(:call1) { create(:call, :international, customer_id: customer2.id) }
        let(:expected) do
          Call::Calculator.new(call2).perform +
            Call::Calculator.new(call3).perform + Invoice::MONTHLY_SUBSCRIPTION
        end
        before do
          its.customers << customer << customer2
          its.calls << call1 << call2 << call3
        end
        let(:invoice) { its.generate_invoice_for(customer.id, month) }

        it { expect(invoice.calls).to eq [call2, call3] }
        it { expect(invoice.customer_id).to eq customer.id }
        it { expect(invoice.month).to eq month }
        it { expect(invoice.charged).to eq(expected) }
      end

      context 'matching two calls from same customer and same month and reject another two' do
        let(:customer2) { create(:customer) }
        let!(:call1) { create(:call, :international, customer_id: customer2.id) }
        let!(:call4) { create(:call, :international, :one_month_later, customer_id: customer.id) }
        before do
          its.customers << customer << customer2
          its.calls << call1 << call2 << call3 << call4
        end
        let(:invoice) { its.generate_invoice_for(customer.id, month) }
        let(:expected) do
          Call::Calculator.new(call2).perform +
            Call::Calculator.new(call3).perform + Invoice::MONTHLY_SUBSCRIPTION
        end

        it { expect(invoice.calls).to eq [call2, call3] }
        it { expect(invoice.customer_id).to eq customer.id }
        it { expect(invoice.month).to eq month }
        it { expect(invoice.charged).to eq(expected) }
      end
    end

    describe 'generate_invoices_for' do
      let(:customer1) { create(:customer) }
      let(:customer2) { create(:customer) }
      let(:call1) { create(:call, :international) }
      let(:call2) { create(:call, :national) }
      let(:month) { call1.month }
      let(:invoices) { its.generate_invoices_for(month) }

      before do
        its.customers << customer1 << customer2
        its.calls << call1 << call2
      end

      it { expect(invoices.size).to eq 2 }
    end
  end
end
