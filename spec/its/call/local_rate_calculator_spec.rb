require 'spec_helper'

module ITS
  class Call
    describe LocalRateCalculator do
      describe '#new' do
        let(:call) { create(:call) }
        let(:rate_calculator) { LocalRateCalculator.new(call) }

        it { expect(rate_calculator.call).to eq call }
      end

      describe '#invoke' do
        let(:rate_calculator) { LocalRateCalculator.new(call) }
        let(:started_at) { call.started_at }
        let(:ended_at) { call.ended_at }

        context 'within weekday' do
          context 'within 8-20 period' do
            let(:call) { create(:call, :local, :wday_within_8_20) }
            let(:expected) { ((ended_at - started_at ) / 60) * LocalRateCalculator::LOCAL_RATE_8_to_20 }

            it { expect(rate_calculator.invoke).to eq expected }
          end
          context 'within 20-8 period' do
            let(:call) { create(:call, :local, :wday_within_20_8) }
            let(:expected) { ((ended_at - started_at) / 60) * LocalRateCalculator::LOCAL_RATE_20_to_8 }

            it { expect(rate_calculator.invoke).to eq expected }
          end
          pending 'round case: mixed 8-20 period and 20-8 period'
        end
        context 'within weekends' do
          let(:call) { create(:call, :local, :weekend) }
          let(:expected) { ((ended_at- started_at) / 60) * LocalRateCalculator::WEEKEND_RATE }
        end
      end
    end
  end
end
