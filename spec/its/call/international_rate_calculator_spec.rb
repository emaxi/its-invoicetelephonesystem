require 'spec_helper'

module ITS
  class Call
    describe InternationalRateCalculator do
      describe '#new' do
        let(:call) { create(:call, :international) }
        let(:rate_calculator){ InternationalRateCalculator.new(call) }

        it { expect(rate_calculator.call).to eq call }
      end
      describe '#invoke' do
        before { Timecop.freeze }

        let(:call) { create(:call, :international) }
        let(:started_at) { call.started_at }
        let(:ended_at) { call.ended_at }
        let(:cost_by_min) { InternationalRateCalculator::RATES[call.destination[0..1]] }
        let(:rate_calculator){ InternationalRateCalculator.new(call) }

        it { expect(rate_calculator.invoke).to eq ((ended_at - started_at) / 60) * cost_by_min }

        after { Timecop.return }
      end
    end
  end
end
