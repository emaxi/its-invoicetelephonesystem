require 'spec_helper'

module ITS
  class Call
    describe NationalRateCalculator do
      describe '#new' do
        let(:call) { create(:call, :international) }
        let(:rate_calculator){ NationalRateCalculator.new(call) }

        it { expect(rate_calculator.call).to eq call }
      end
      describe '#invoke' do
        before { Timecop.freeze }

        let(:call) { create(:call, :national) }
        let(:started_at) { call.started_at }
        let(:ended_at) { call.ended_at }
        let(:cost_by_min) { NationalRateCalculator::RATES[call.destination[2..5]] }
        let(:rate_calculator){ NationalRateCalculator.new(call) }

        it { expect(rate_calculator.invoke).to eq ((ended_at - started_at) / 60) * cost_by_min }

        after { Timecop.return }
      end
    end
  end
end
