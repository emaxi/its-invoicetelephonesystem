require 'spec_helper'

module ITS
  class Call
    describe Calculator do
      describe '.perform' do
        let(:call) { create(:call) }
        let(:fake_calculator) { spy('fake_calculator') }
        before do
          allow(RateStrategySelector).to receive(:for).with(call) { fake_calculator }
          Calculator.new(call).perform
        end
        it { expect(RateStrategySelector).to have_received(:for).with(call) { fake_calculator } }
        it { expect(fake_calculator).to have_received(:invoke) }
      end
    end
  end
end
