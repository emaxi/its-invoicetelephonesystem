require 'spec_helper'

module ITS
  class Call
    describe RateStrategySelector do
      describe '#for' do
        context 'international call' do
          let(:call) { create(:call, :international) }
          it { expect(RateStrategySelector.for(call).is_a?(InternationalRateCalculator)).to be_truthy }
        end
        context 'national call' do
          let(:call) { create(:call, :national) }
          it { expect(RateStrategySelector.for(call).is_a?(NationalRateCalculator)).to be_truthy }
        end
        context 'local call' do
          let(:call) { create(:call, :local) }
          it { expect(RateStrategySelector.for(call).is_a?(LocalRateCalculator)).to be_truthy }
        end
      end
    end
  end
end
