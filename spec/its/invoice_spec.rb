require 'spec_helper'

module ITS
  describe Invoice do
    describe '#new' do
      let(:id) { create(:customer).id }
      let(:call1) { create(:call) }
      let(:call2) { create(:call) }
      let(:calls) { [call1, call2] }
      let(:month) { 2 }
      let(:calculator) { Call::Calculator }
      let(:invoice) { Invoice.new(id, calls, month, calculator) }

      it { expect(invoice.customer_id).to eq id }
      it { expect(invoice.calls).to eq calls }
      it { expect(invoice.month).to eq month }
    end

    describe '#charged' do
      let(:invoice) { create(:invoice) }
      let(:expected) do
        invoice.calls.map { |call| Call::Calculator.new(call).perform.round(2) }.reduce(&:+) + Invoice::MONTHLY_SUBSCRIPTION
      end

      it { expect(invoice.charged).to eq expected }
    end

    pending '#calculate_call'

    pending '#calculate_calls'

    pending 'call_detail'

    pending '#to_s'
  end
end
