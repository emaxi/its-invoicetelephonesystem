require 'spec_helper'

module ITS
  describe Call do
    describe '#new' do
      let(:id) { create(:customer).id }
      let(:destination) { Faker::PhoneNumber.phone_number }
      let!(:started_at) { Time.now }
      let!(:ended_at) { Time.now + 3600 }
      let(:call) { Call.new(id, destination, started_at, ended_at) }

      it { expect(call.customer_id).to eq(id) }
      it { expect(call.ended_at).to eq(ended_at)}
      it { expect(call.destination).to eq(destination) }
      it { expect(call.started_at).to eq(started_at)}
    end
  end
end
