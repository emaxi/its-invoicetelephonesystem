require 'spec_helper'

module ITS
  describe Customer do
    describe '#new' do
      let(:phone) { Faker::PhoneNumber.phone_number }
      let(:full_name) { Faker::Name.name }
      let(:customer) { Customer.new(phone, full_name) }

      it { expect(customer.id).to_not be_nil }
      it { expect(customer.phone).to eq phone }
      it { expect(customer.full_name).to eq full_name }
    end
  end
end
